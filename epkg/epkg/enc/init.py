def run(hub):
    """
    Run the specified encryption from the config
    """
    hub.epkg.enc[hub.OPT.epkg.enc].encrypt()
