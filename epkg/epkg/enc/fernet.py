from cryptography.fernet import Fernet


def encrypt(hub):
    """
    Using fernet, encrypt the prepared archive
    """
    hub.epkg.ENC = "fernet"
    key = Fernet.generate_key()
    f = Fernet(key)
    hub.epkg.TOKEN = f.encrypt(hub.epkg.BYTES.read())
    hub.epkg.KEY = key


def decrypt(hub, token, key):
    """
    Decrypt the data and return it
    """
    f = Fernet(key)
    return f.decrypt(token)
