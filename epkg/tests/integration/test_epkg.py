import os
import pathlib
import subprocess
import sys
import tempfile

import pytest


@pytest.fixture(scope="module")
def code_dir() -> pathlib.Path:
    return pathlib.Path(__file__).parent.parent.parent.parent.absolute()


@pytest.fixture(scope="module")
def run_py(code_dir) -> str:
    return str(code_dir.joinpath("run.py"))


@pytest.fixture(scope="module")
def output_file():
    with tempfile.NamedTemporaryFile(suffix=".epkg") as f:
        yield f.name


def test_create_and_display(run_py, output_file):
    proc = subprocess.Popen(
        [
            sys.executable,
            run_py,
            "create",
            f"--output={output_file}",
        ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    assert proc.wait() == 0
    stderr = proc.stderr.read().decode()
    assert not stderr, stderr
    assert os.path.getsize(output_file) > 0

    # Fetch key from output
    outputs = proc.stdout.readlines()
    assert 2 == len(outputs)
    assert output_file in outputs[0].decode()
    key = outputs[1].decode().split(": ")[-1]

    # Fetch decrypted list of files from output file
    proc = subprocess.Popen(
        [sys.executable, run_py, "display", f"{output_file}", f"{key}"],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )

    assert proc.wait() == 0
    stderr = proc.stderr.read().decode()
    assert not stderr, stderr
    # output lists all the files/directories
    output = proc.stdout.readlines()
    assert 1 < len(output)
